all:
	./indexphotos.py; ./createIndex.py
	find */mp3* -name "*.mp3" -exec chmod a+r {} \;

force_all:
	for f in $$(find . -name '*.mscz'); do touch "$$f"; done
	./indexphotos.py; ./createIndex.py

rsync_upload: all
	rsync -av --exclude Makefile --exclude "*~" --exclude templates/ \
	   --exclude "*.py" --exclude "__pycache__/" --delete * \
	   freeduc.science:/var/www/html/chorale/Aap5Ac7E/

.PHONY: all rsync_upload force_all
