#! /usr/bin/python3
import glob, os
from jinja2 import Environment, FileSystemLoader
from PIL import Image
from subprocess import run

# no more pixels than in an image 1024*768
MAX_PIXEL = 8e5

TEMPLATES = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    "templates"
)

J2E = Environment(loader=FileSystemLoader(TEMPLATES), trim_blocks=True)

def render(filename, context):
    return J2E.get_template(filename).render(context)

def mkindex(d):
    paths = glob.glob(d+"/*.[jJ][pP][gG]")
    photos = [os.path.basename(f) for f in paths]
    for p in paths:
        img=Image.open(p)
        w,h = img.size
        pixels = w*h
        if pixels > MAX_PIXEL:
            scale = (MAX_PIXEL/pixels)**0.5
            new = img.resize((int(w*scale),int(h*scale)))
            new.save(p)
            print("redimensionné", p)
    paths = []
    for ext in ("webm", "mov", "MOV", "mp4", "MP4", "VOB"):
        paths +=glob.glob(d+"/*."+ext)
    for p in paths:
        if "mini" in p:
            continue
        minifile = os.path.splitext(p)[0]+"_mini.webm"
        if not os.path.exists(minifile):
            cmd = [
                "ffmpeg", "-i", p,
                "-af", 'aformat=channel_layouts="5.1|stereo"',
                "-c:v", "libvpx-vp9",
                "-crf",  "30",
                "-b:v", "0",
                "-b:a", "128k", 
                "-c:a", "libopus", minifile
            ]
            cp = run(" ".join(cmd), shell=True)
            print("rééchantillonné :", minifile)
            print("avec la commande", " ".join(cmd))
            if cp.returncode == 0:
                run(["rm", p])
                print("effacé :", p)
    videos = [os.path.basename(f) for f in glob.glob(d+"/*mini*.webm")]
    html =render("photoindex.html", {
        "photos": photos,
        "videos": videos,
        "dir": d,
        "title": "diaporama",
    })
    with open(os.path.join(d,"index.html"), "w") as outfile:
        outfile.write(html)
    print(f"""mise à jour de {os.path.join(d,"index.html")}""")
    
if __name__ == "__main__":
    photodirs = sorted(glob.glob("photos-*"))
    for d in photodirs:
        mkindex(d)
