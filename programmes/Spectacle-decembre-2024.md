---
title: Concert Spectacle Décembre 2024
---

# Au programme :

## Chants de Noël
  - Mon beau sapin
  - les anges de nos campagnes
  - dans une étable obscure
  - vive le vent
  - somewhere in my memory
  - petit papa noel
  - la marche des rois
  - [Carol of the bells](https://www.youtube.com/watch?v=SQadcm_dwEM)
  
## Chants comédy musicale

  - Roméo et juliette   - les rois du monde
  - L'envie d'aimer
  - Over the rainbow   - le magicien d'Oz
  - La belle et la bête   - c'est la fête + chansons éternelles
  - Le roi lion - l'histoire de la vie
  - [Marry you   - Bruno Mars (version glee)](https://www.youtube.com/watch?v=vP2eGuTgpak)
  - Berühmtheit
  - [Don't stop believin'](https://www.youtube.com/watch?v=JseWhrUz9TY)
  - Oh ! Happy Day ! (gospel   - sister act)
  - Starmania   - quand on arrive en ville
  - Notre Dame de Paris   - Belle ou Le temps des cathédrales.
  
  - Total éclipse of the Heart – Bonnie Tyler peut être.