// Hook `click` on the table header, but only call our callback if
// that click passes through a `th`

function make_sortable(){
    $(".sortable thead").on("click", "th", function() {
	// Which column is this?
	var th = $(this);
	var index = th.index();
	// Sorting vs. sorting in reverse order
	var reverse = null;
	if (!th.hasClass("sorted") && !th.hasClass("reversed")){
	    // the column has not yet been touched; consider the preference
	    if (th.hasClass("prefer-reversed")){
		th.addClass("reversed");
		reverse = true;
	    } else {
		th.addClass("sorted");
		reverse = false;
	    }
	} else {
	    // the column has been ordered previously; changing this order
	    if (th.hasClass("sorted")){
		th.removeClass("sorted");
		th.addClass("reversed");
		reverse = true;
	    } else {
	    th.removeClass("reversed");
		th.addClass("sorted");
		reverse = false;
	    }
	}	
	// Get the tbody
	var tbody = $(this).closest("table").find("tbody");
	
	// Disconnect the rows and get them as an array
	var rows = tbody.children().detach().get();
	
	// Sort it
	rows.sort(function(left, right) {
	    // Get the text of the relevant td from left and right
	    var $left = $(left).children().eq(index);
	    var $right = $(right).children().eq(index);
	    return $left.text().localeCompare($right.text());
	});

	if (reverse) rows.reverse();
	
	// Put them back in the tbody
	tbody.append(rows);
    });
}
