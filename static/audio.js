/**
 * La façon d'associer un bouton radio à des valeurs et à une couleur
 * les valeurs sont 0, 1, 2 ou 3
 **/
var radioValeurs = [
    {name: "top", val: 85, colorClass: "greenRadio", title: "+++"},
    {name: "maj", val: 70, colorClass: "yellowRadio", title: "++"},
    {name: "iso", val: 50, colorClass: "orangeRadio", title: "-"},
    {name: "min", val: 30, colorClass: "redRadio", title: "--"},
];

/**
 * @var theSliders la liste des sliders
 **/
var theSliders =[];

function timeStr(t){
    h = Math.floor(t/3600);
    t = t - 3600 * h;
    m = Math.floor(t/60);
    t = t - 60*m;
    s = Math.floor(t);
    s = "0" + s;
    if (s.length > 2) s = s.substring(1);
    t = t - s;
    d = Math.floor(10*t);
    var result = "";
    if (h) result += h + ":";
    result += m + ":";
    result += s + "." + d;
    return result
}

function val2time(val){
    var regexp = /(?:(?:(?<hh>\d{1,2})[:])?(?<mm>\d{1,2})[:])?(?<ss>\d{1,2})(\.(?<ff>\d*)?)?/;
    var m = val.match(regexp);
    var result = 0;
    if (m){
	if (typeof m.groups["hh"] != "undefined")
	    result += 3600 * parseInt(m.groups["hh"]);
	if (typeof m.groups["mm"] != "undefined")
	    result += 60 * parseInt(m.groups["mm"]);
	if (typeof m.groups["ss"] != "undefined")
	    result += parseInt(m.groups["ss"]);
	if (typeof m.groups["ff"] != "undefined")
	    result += parseFloat("0."+m.groups["ff"]);
    }
    return result;
}

/**
 * Crée un nouveau bouton pour lancer les lectures audio
 * sauf s'il existe déjà un bouton pour exactement le même temps.
 * @param button le bouton qui a été cliqué pour lancer cette création
 * @param t le timbre à date pour le lancement des audios
 * @param pointsArretBox l'élément fieldset où on ajoute le nouveau bouton
 * @param title un titre éventuel pour le nouveau bouton
 **/
function creeEntree(button, t, pointsArretBox, title){
    var dejaButtons = pointsArretBox.querySelectorAll("button.rejoue");
    var addButton = pointsArretBox.querySelector("button.add");
    for (var i = 0; i < dejaButtons.length; i++){
	// pas de nouveau bouton si on en a déjà un au même temps
	if (dejaButtons[i].getAttribute("data-t") == "" + t) return;
    }
    var b = document.createElement("button")
    if (typeof(title) !== "undefined" && title.length){
	b.innerHTML = " " + title;
    } else {
	b.innerHTML = timeStr(t);
    }
    b.setAttribute("data-t", t);
    b.setAttribute("title", "Jouer à partir de " + timeStr(t));
    b.setAttribute("onclick", 'rejoue(this, '+t+')');
    if (button.classList.contains("toutes")){
	b.setAttribute("class", 'rejoue toutes');
    } else {
	b.setAttribute("class", 'rejoue');
    }
    for (var i = 0; i < dejaButtons.length; i++){
	// insertion avant un bouton plus tardif
	if (parseFloat(dejaButtons[i].getAttribute("data-t")) >  t){
	    pointsArretBox.insertBefore(b, dejaButtons[i]);
	    return;
	}
    }
    pointsArretBox.insertBefore(b, addButton);
}

function closeStaticInput(button){
    var parent= button.parentNode;
    while (! parent.tagName.match(/td/i)) parent = parent.parentNode;
    var input = parent.querySelector(".static_add");
    if (input){
	input.setAttribute("style", "display: none");
	input.value="";
    }
}

/**
 * crée un nouveau point d'entrée, basé sur le temps courant du premier
 * élément audio pertinent
 * @param button le bouton qui a été cliqué
 * @param title un intitulé optionnel pour le futur point d'entrée
 **/
function pointEntree(button, title){
    var parent=button.parentNode;
    while (! parent.tagName.match(/td/i)) parent = parent.parentNode;
    var audio;
    var input = parent.querySelector(".static_add");
    var pointsArretBox = parent.querySelector("fieldset.temps");
    if (button.classList.contains("toutes")){
	var summary = pointsArretBox.querySelector("input[type=hidden]").value;
	summary = summary.replace(/ */g, "").replace(/'/g,'"');
	summary = (JSON.parse(summary));
	for (tag in summary){
	    audio = document.getElementById(summary[tag]);
	    break; /* seulement le premier audio */
	}
    }
    creeEntree(button, audio.currentTime, pointsArretBox, title);
}

/**
 * Met à jouer une ou plusieurs voix, à un temps t
 * Réaffiche le symbole de pause sur le bouton de gauche
 * @param buttonEntree le bouton qui a été cliqué
 * @param t le timbre à date pour lancer le rendu des voix
 **/
function rejoue (buttonEntree, t){
    closeStaticInput(buttonEntree);
    var parent = buttonEntree.parentNode.parentNode;
    var playPauseButton = parent.querySelector("button.stop");
    var img = playPauseButton.querySelector("img");
    if (img.src.match(/play/)){
	img.src = img.src.replace("play","pause");
    }
    var audio;
    if (buttonEntree.classList.contains("toutes")){
	var summary = parent.querySelector("input[type=hidden]").value;
	summary = summary.replace(/ */g, "").replace(/'/g,'"');
	summary = (JSON.parse(summary));
	for (tag in summary){
	    audio = document.getElementById(summary[tag]);
	    audio.currentTime = t;
	    audio.play();
	}
    } else {
	audio = parent.querySelector("audio");
	audio.currentTime = t;
	audio.play();
    }
}

/**
 * fonction de rappel pour la sélection de mesures
 * @param button le bouton qui a été cliqué
 **/
function go_mesure(button){
    var select = button.previousElementSibling;
    var option = select.options[select.selectedIndex];
    var timestamp=select.value;
    var parent=button.parentNode;
    while (! parent.tagName.match(/td/i)) parent = parent.parentNode;
    var audios = parent.querySelectorAll("audio");
    if (audios.length == 0)
	// pas d'audio dans la même case ;
	// on cherche dans le première case de la ligne suivante ...
	parent = parent.parentNode.nextElementSibling.querySelector("td");
    audios = parent.querySelectorAll("audio");
    audios.forEach(function(audio){
	audio.currentTime = timestamp;
    });
    var pointsArretBox = button.parentNode.nextElementSibling;
    var plusButton = pointsArretBox.querySelector("button.add");
    creeEntree(plusButton, timestamp, pointsArretBox, "M. "+ option.text);
 }

/**
 * Lance la lecture ou met en pause pour une voix ou plusieurs
 * @param button le bouton qui a été cliqué. Ses attributs sont modifiés
 * à la façon d'une bascule à chaque clic.
 **/
function playpausevoix (button){
    var parent = button.parentNode.parentNode;
    var img = button.querySelector("img");
    var play = (img.src.match(/play/) != null);
    if (play){
	img.src = img.src.replace("play", "pause");
    } else {
	img.src = img.src.replace("pause", "play");
    }
    var audio;
    if (button.classList.contains("toutes")){
	var summary = parent.querySelector("input[type=hidden]").value;
	summary = summary.replace(/ */g, "").replace(/'/g,'"');
	summary = (JSON.parse(summary));
	for (tag in summary){
	    audio = document.getElementById(summary[tag]);
	    if (play){
		audio.play();
	    } else {
		audio.pause();
	    }
	}
    } else {
	audio = parent.querySelector("audio");
	if (play){
	    audio.play();
	} else {
	    audio.pause();
	}
    }
}


/* vertical sliders' source:
   https://pygmalion.nitri.org/a-vertical-slider-in-html-js-css-1083.html 
*/

/**
 * Crée un potentiomètre vertical
 * @param min valeur minimale
 * @param max valeur maximale
 * @param tag une étiquette pour le potentiomètre
 * @param ident un identifiant unique
 * @return un élément DIV contenant le nécessaire pour le potentiomètre
 **/
function Slider(min, max, tag, title, ident) {
    this.min = min;
    this.max = max;
    this.value = min;
    this.container = document.createElement("div");
    this.container.classList.add("slider_container");
    this.container.setAttribute("data-target", ident);
    this.container.setAttribute("title", "Contrôle pour " + title);
    this.container.classList.add("orangeRadio");
    this.target = ident;
    this.vdiv = document.createElement("div");
    this.container.append(this.vdiv);
    this.element = document.createElement("div");
    this.element.classList.add("vslider");
    this.element.style.height = "100px";
    this.vdiv.append(this.element);
    for(var i=0; i < radioValeurs.length; i++){
	var radio = document.createElement("input");
	radio.setAttribute("type", "radio");
	radio.setAttribute("class", "r"+i);
	radio.setAttribute("id", "r"+ident+"_"+i);
	radio.setAttribute("name", "r"+ident);
	var fun = function(a,b){
	    return "volumes('"+a+"',"+b+")";
	}
	radio.setAttribute("onchange", "volumes(this,'"+ident+"',"+i+")");
	if(i==2){
	    radio.setAttribute("checked", "1");
	}
	var label = document.createElement("label");
	label.setAttribute("class", "r"+i);
	label.setAttribute("for", "r"+ident+"_"+i);
	label.setAttribute("title", radioValeurs[i].title);
	/* calcul de top: 100 (hauteur du parent) - valeur - rayon */
	label.style.top = ""+(100 - radioValeurs[i].val - 2.5)+"px"
	this.element.append(radio);
	this.element.append(label);
    }
    this.thumb = document.createElement("div");
    this.thumb.classList.add("thumb");
    this.element.append(this.thumb);
    this.tag = document.createElement("div");
    this.tag.innerHTML = tag;
    this.tag.classList.add("tag");
    this.display = document.createElement("div");
    this.display.classList.add("vdisplay");
    this.vdiv.append(this.display);
    this.vdiv.append(this.tag);
    this.shift = 5; // demi-hauteur du curseur
    
 
    var sliderInstance = this;
 
    var mouseDownCallback = function (evt) {
 
        var thumbYOffset = evt.clientY - sliderInstance.thumb.offsetTop;
 
        var mouseMoveCallback = function (evt) {
            var yRange = sliderInstance.element.offsetHeight;
            var y = Math.max(0, Math.min(yRange, evt.clientY - thumbYOffset));
            sliderInstance.thumb.style.top = y - sliderInstance.shift + 'px';
            sliderInstance.value = max - y / yRange * (max - min);
            sliderInstance.onChange();
	    /* remet à la couleur "iso" les conteneurs de sliders */
	    var sliders = sliderInstance.container.parentNode.querySelectorAll("div.slider_container");
	    [...sliders].forEach(s => {
		radioValeurs.forEach(rv => {
		    /* remet la couleur du conteneur à la couleur "iso" */
		    if (rv.name != "iso"){
			s.classList.remove(rv.colorClass);
		    } else {
			s.classList.add(rv.colorClass);
		    }
		});
		/* decoche tous ses boutons radio */
		var buttons = s.querySelectorAll("input[type=radio]");
		[...buttons].forEach(b => b.checked = false);
	    });
            evt.preventDefault();
        };
 
        var mouseUpCallback = function (evt) {
            document.removeEventListener('mousemove', mouseMoveCallback, false);
            document.removeEventListener('mouseup', mouseUpCallback, false);
        };
 
        document.addEventListener('mousemove', mouseMoveCallback, false);
        document.addEventListener('mouseup', mouseUpCallback, false);
 
        evt.preventDefault();
    };
 
    this.thumb.addEventListener('mousedown', mouseDownCallback, false);
}

/**
 * donne une valeur au slider
 **/
Slider.prototype.setValue = function (value) {
    var value = Math.max(this.min, Math.min(this.max, value));
    this.value = value;
    this.onChange();
};
 
/**
 * place le curseur à l'endroit qui correspond à une valeur
 * @param value la valeur
 **/
Slider.prototype.setCursor = function (value) {
    var value = Math.max(this.min, Math.min(this.max, value));
    var yRange = parseInt(this.element.style.height);
    var y = Math.floor((this.max - value) / (this.max - this.min) * yRange);
    this.thumb.style.top =  "" + (y - this.shift) + 'px';
};
 
Slider.prototype.getValue = function () {
    return this.value;
};
 
Slider.prototype.getId = function () {
    return this.element.id;
};

/**
 * ajuste les volumes de tous les sliders, à commencer par celui qui
 * est identifié
 * @param elt l'élément iput qui a déclenché cette fonction
 * @param ident l'identifiant du slider
 * @param val 0, 1, 2 ou 3
 **/
function onSliderChange() {
    var slider=this;
    /* le volume de l'audio est modifié par une fonction de rappel*/
    window.setTimeout(
	function(){
	    var audio = document.querySelector("#" + slider.target);
	    var pcent = slider.getValue().toFixed(0);
	    slider.display.innerHTML = pcent;
	    audio.volume = pcent/100;
	},
	0
    );
}

/**
 * retrouve un objet slider à partir de son identifiant
 * @param ident l'identifiant
 * @return un objet Slider
 **/
function id2slider(ident){
    var found = theSliders.filter(s => s.target == ident);
    if (found) return found[0]; else return null;
}

function newSlider(tag, title, ident, parentid){
    var slider = new Slider(0, 100, tag, title, ident);
    theSliders.push(slider);
    slider.onChange = onSliderChange;
    slider.setValue(25);
    document.getElementById(parentid).append(slider.container);
}

function volumes(elt,ident, val){
    var sliderBox = elt.parentNode.parentNode.parentNode.parentNode;
    var sliders = sliderBox.querySelectorAll(".slider_container");
    var thisSlider = sliderBox.querySelector(".slider_container[data-target="+ident+"]");
    var otherSliders = [...sliders].filter(s => s != thisSlider);
    var sliderObj = id2slider(thisSlider.getAttribute("data-target"));
    var cursor = sliderObj.thumb;
    var value = radioValeurs[val].val;
    /* règle le curseur et la valeur */
    sliderObj.setValue(value);
    sliderObj.setCursor(value);
    /* décolore puis recolore le conteneur */
    radioValeurs.forEach(r => thisSlider.classList.remove(r.colorClass));
    thisSlider.classList.add(radioValeurs[val].colorClass);
    otherSliders.forEach(container => {
	/* règle le curseur et la valeur */
	var s = id2slider(container.getAttribute("data-target"));
	s.setValue(100-value);
	s.setCursor(100-value);
	/* décolore le conteneur, décoche les boutons radio */
	radioValeurs.forEach(r => {
	    container.classList.remove(r.colorClass);
	    var radios = container.querySelectorAll("input[type=radio]");
	    [...radios].forEach(r => r.checked=false);
	});
	/* recolore le conteneur si c'est le niveau "iso" */
	if(radioValeurs[val].name == "iso"){
	    container.classList.add(radioValeurs[val].colorClass);
	}

    });
}

function init(summary, parentid) {
    for (tag in summary){
	var sum = JSON.parse(summary[tag]);
	newSlider(tag, sum.title, sum.ident, parentid);
    }
}


/**
 * gestion de certaines aides comme des panneaux en accordéon
 * ==========================================================
 * les éléments de classe "accordeon" sont censés contenir un
 * élément <h3> au début, suivi de plusieurs autre éléments.
 * le clic sur l'élément <h3> permet de voir/cacher les éléments
 * suivants.
 **/

var accordeons;

function display_elements(elt, val){
    if (! elt) return;
    elt.setAttribute("style", "display:"+val);
    display_elements(elt.nextElementSibling, val);
}

function toggle_accordeon(evt){
    var elt = evt.target;
    var parent = elt.parentElement;
    var contents = parent.querySelectorAll("*");
    if (elt.classList.contains("replie")){
	contents.forEach(function(item){
	    item.classList.remove("replie");
	    item.classList.add("deplie");
	});
	display_elements(elt.nextElementSibling,"visible");	
    } else {
	contents.forEach(function(item){
	    item.classList.remove("deplie");
	    item.classList.add("replie");
	});
	display_elements(elt.nextElementSibling,"none");
    }
}

function init_accordeons(){
    accordeons = document.querySelectorAll(".aide");
    accordeons.forEach(function(elt){
	var h3=elt.querySelector("h3");
	h3.classList.add("replie", "accordeon-titre");
	display_elements(h3.nextElementSibling,"none");
	h3.addEventListener("click", toggle_accordeon);
    });
}
