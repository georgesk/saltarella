// -*- mode: javascript -*-
/**
 * *******************************************************
 * Attention : il faut recopier ce fichier dans un chemin
 * valide pour les plugins de Musescore, par exemple dans
 * le chemin ~/Documents/MuseScore2/Plugins/
 * *******************************************************
 *
 * Ce fichier tempo.qml est un plugin pour Musescore, qui permet
 * de produire des lignes de message, au sujet du tempo d'un fichier
 * musical. Il produit au minimum :
 *
 * - une ligne "qml: duration <temps en secondes>"
 * - une ligne "qml: nmeasures <nombre de mesures>"
 *
 * Ces lignes sont faciles à filtrer ensuite, par exemple de la façon
 * suivante :
 * *******************************************************
 * musescore -p tempo.qml Imagin.mscz 2>&1 | grep -v "removeSpanner"
 * *******************************************************
 **/
import QtQuick 2.0
import MuseScore 1.0

MuseScore {
      menuPath: "Plugins." + qsTr("Mon tempo")
      description: "Renseignements temporels (durée, nombre de mesures)"
      version: "1.0"
      onRun: {
            console.log("Plugin tempo.qml")
            var score = curScore
            console.log("duration",score.duration)
            console.log("nmeasures", score.nmeasures)
      }
}
