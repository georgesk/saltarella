#! /usr/bin/python3

import os, sys, json
from jinja2 import Environment, FileSystemLoader
import xml.etree.ElementTree as ET
from collections import OrderedDict
import datetime
import locale
import re
import tempfile
import glob
from subprocess import run
from markupsafe import Markup
import zipfile
import xml.etree.ElementTree as ET

TEMPLATES = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    "templates"
)

TIMECODE = re.compile(r"timecode:\s+(?P<time>[.:\d]+)\s*.*,\s*(?P<comment>.*)")

J2E = Environment(loader=FileSystemLoader(TEMPLATES), trim_blocks=True)

def toTimecode(tc):
    """
    à partir d'un timecode tel que "12:03.05", fabrique un flottant 723.05
    @param tc un time code sous forme de chaîne
    @return une durée en secondes (flottant)
    """
    hms = tc.split(":");
    index = len(hms) - 1
    mul = 1
    result = 0.0
    while index >=0:
        result += mul * float(hms[index])
        index -= 1
        mul *= 60
    return result

def render(filename, context):
    return J2E.get_template(filename).render(context)

def readableSize (s):
    if s < 1024:
        return f"{s} octets"
    elif s < 1024*1024:
        return f"{s/1024:6.1f} Ko"
    return f"{s/1024/1024:6.1f} Mo"

class VideoLink:
    """
    Une classe pour enregistrer le chemin et la date de mise en ligne
    d'une vidéo
    """

    def __init__(self, path):
        self.path = path
        self.date = datetime.datetime.fromtimestamp(os.path.getmtime(path),
                                                    datetime.UTC)
        self.datestr = self.date.strftime("%Y-%m-%d")
        return

    def __str__(self):
        return f"Videolink to {self.path} ({self.date})"

    def __repr__(self):
        return str(self)
    
class Audio:
    """
    Une classe pour fabriquer un élément audio

    paramètres du constructeur:
    :param: sndfile un élément <sndfile> digéré par xml.etree.ElementTree
    :param: manifestdir chemin vers la racine des fichiers
    """

    def __init__(self, sndfile, manifestdir):
        self.title = sndfile.find("./title").text
        self.path = os.path.join(manifestdir, sndfile.find("./file").text)
        self.ident = f"A_{id(self)}"
        if "summary" in sndfile.attrib:
            self.tag = sndfile.attrib["summary"]
        else:
            self.tag= ""
        return

    @property
    def toHTML(self):
        return f"""\
<audio id="{self.ident}" controls class="audio-control">
    <source src="{self.path}" />
    Votre navigateur ne supporte pas l'élément audio
</audio>
"""
    def to_json(self):
        return json.dumps({
            "tag": self.tag, "title": self.title, "ident": self.ident})
    
class MP3set:
    """
    une classe pour une collection de fichiers MPS
    """

    def __init__(self, root):
        try:
            self.manifest=os.path.join(root, "manifest.xml")
            self.source = ""
            self.duration, self.nmeasures = 0, 0
            self.tree = ET.parse(self.manifest)
            self.uniqueId = f"A_{id(self)}"
            self.root = os.path.dirname(self.manifest)
            self.manifestdir = os.path.basename(self.root)
            self.dir = os.path.dirname(self.root)
            self.doc = self.tree.getroot()
            self.source = self.findSource()
            self.entrees = self.findEntrees()
            self.title = self.doc.find("./title").text
            self.collection = OrderedDict()
            self.size = 0
            self.date = datetime.datetime(year = 2000, month=1, day=1)
            for elt in self.doc.findall("./sndfile"):
                ftitle = elt.find("./title").text
                self.collection[ftitle] = Audio(elt, self.manifestdir)
                file_ = os.path.join(self.root, elt.find("./file").text)
                self.size = max (self.size,
                                 os.path.getsize(file_))
                self.date = max( self.date,
                                 datetime.datetime.fromtimestamp(os.path.getmtime(file_)))
            self.size = readableSize(self.size)
        except Exception as err:
            msg = f"Erreur : {err}, avec le chemin {root}"
            raise Exception(msg)
        return

    def findSource(self):
        """
        Recherche s'il y a un fichier source musescore
        @return le nom du fichier source s'il existe, sinon None
        """
        sourceElement = self.doc.find("./source")
        result = None
        path = ""
        if sourceElement is not None:
            path = sourceElement.text.strip()
            abspath = os.path.join(self.dir, path)
            if os.path.exists(abspath):
                result = abspath
        return result

    def findEntrees(self):
        """
        Trouve les timecodes mis dans le fichiser musescore, s'il y en a.
        Pour mettre un time code, on insère un texte de portée (staff text)
        avec la syntaxe "timecode: mm:ss.cc, commentaire"
        @return un dictionnaire
           timecode flottant -> { tc: timecode en texte comment: commentaire}
        """
        result = {}
        if self.source:
            with zipfile.ZipFile(self.source) as myzip:
                path = [f for f in myzip.namelist()
                             if f.endswith('.mscx')][0]
                with myzip.open(path) as musicfile:
                    tree = ET.parse(musicfile)
                    st = [s.find('text').text for s in tree.findall('.//StaffText')]
                    for s in st:
                        m = TIMECODE.match(s)
                        if m:
                            result[toTimecode(m.group("time"))] = {
                                "tc": m.group("time"),
                                "comment": m.group("comment"),
                            }
        return result
        
    @property
    def summary(self):
        result=OrderedDict()
        for audio in self.collection.values():
            if audio.tag:
                result[audio.tag] = audio.to_json()
        return result

    @property
    def summary_simple(self):
        result=OrderedDict()
        for audio in self.collection.values():
            if audio.tag:
                result[audio.tag] = audio.ident
        return result

    def __str__(self):
        return f"""\
MP3set(title="{self.title}, size={self.size}", files=[{", ".join([str(i) for i in self.collection.items()])}])"""

    @property
    def getSummary(self):
        return {k :v for k, v in self.summary.items()}
    
    @property
    def getSummary_simple(self):
        return {k :v for k, v in self.summary_simple.items()}

class mscFile:
    def __init__(self, path, size):
        self.path = path
        self.size = readableSize(size)
        return
    def __str__(self):
        return "fichier MSCZ " + self.path
    
class mxmlFile(mscFile):
    def __str__(self):
        return "fichier MUSICXML " + self.path

    
def createSubIndex(path0, title, dirty=True):
    """
    Crée le fichier index.html dans un sous-répertoire
    @param path0 le sous-répertoire
    @param title le titre
    @param dirty il faut refaire le fichier index.html (vrai par défaut)
    """
    msc=[]
    mscsize = 0
    musicxml = []
    musicxmlsize = 0
    pdf=[]
    pdfsize = 0
    mp3sets=[]
    if not dirty:
        date = datetime.datetime.fromtimestamp(os.path.getmtime(
            os.path.join(path0, "index.html")))
        print(f"Déjà à jour : {path0}/index.html")
        return date
    date=datetime.datetime(year=2000, month=1, day=1)
    for root, dirs, files in os.walk(path0):
        for name in files:
            if name.endswith(".mscz"):
                path = os.path.join(root, name)
                date = max(date,
                           datetime.datetime.fromtimestamp(os.path.getmtime(
                               path)))
                msc.append(mscFile(os.path.basename(path), os.path.getsize(path)))
                s = os.path.getsize(path)
                mscsize = max(s, mscsize)
            elif name.endswith(".musicxml"):
                path = os.path.join(root, name)
                date = max(date,
                           datetime.datetime.fromtimestamp(os.path.getmtime(
                               path)))
                musicxml.append(os.path.basename(path))
                s = os.path.getsize(path)
                musicxmlsize = max(s, mscsize)
            elif name.endswith(".pdf"):
                path = os.path.join(root, name)
                date = max(date,
                           datetime.datetime.fromtimestamp(os.path.getmtime(
                               path)))
                pdf.append(os.path.basename(path))
                s = os.path.getsize(path)
                pdfsize = max(s, pdfsize)
        if os.path.basename(root).startswith("mp3_") and \
           "manifest.xml" in files:
            mp3s = MP3set(root)
            # !!!ici il faut fabriquer éventuellement un widget
            # pour aller à telle ou telle mesure
            date = max(date, mp3s.date)
            mp3sets.append(mp3s)
        mp3sets.sort(key=lambda x: x.root)
    with open(os.path.join(path0, "index.html"), "w") as outfile:
        outfile.write(render("index.html",{
            "title": title,
            "msc": msc,
            "mscsize": readableSize(mscsize),
            "musicxml": musicxml,
            "musicxmlsize": readableSize(musicxmlsize),
            "pdf": sorted(pdf),
            "pdfsize": readableSize(pdfsize),
            "mp3sets": mp3sets,
        }))
    print(f"Création de : {path0}/index.html")
    return date

def findVideos():
    """
    renvoie un dictionnaire année => lien vidéo
    """
    liste = [VideoLink(path) for path in glob.glob("vide*/*.mp4")] + \
        [VideoLink(path) for path in glob.glob("photo*/*.mp4")] + \
        [VideoLink(path) for path in glob.glob("vide*/*.webm")] + \
        [VideoLink(path) for path in glob.glob("photo*/*.webm")]
    result={}
    for v in sorted(liste, key = lambda v: v.date, reverse = True):
        if v.date.year in result:
            result[v.date.year].append(v)
        else:
            result[v.date.year] = [v]
    return result

def hasPhotos(path):
    return glob.glob(path+"/*.[jJ][pP][gG]")

def must_create_index(path0):
    """
    Vérifie que le fichier index.html soit à jour
    @param path0 un sous-répertoire
    @return vrai si index n'est pas plus récent que tous les autres fichiers
    """
    if not os.path.exists(os.path.join(path0, 'index.html')):
        return True # if faut créer index.html comme il est absent
    date=datetime.datetime(year=2000, month=1, day=1)
    for root, dirs, files in os.walk(path0):
        for name in files:
            path = os.path.join(root, name)
            date = max(date,
                       datetime.datetime.fromtimestamp(os.path.getmtime(path)))
    path = os.path.join(path0, 'index.html')
    return date > datetime.datetime.fromtimestamp(os.path.getmtime(path))
        
def createIndex():
    """
    Création des fichiers index.html, et exportation éventuelle de
    fichiers utiles
    """
    locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
    foundDirs = {}
    for path in os.listdir("."):
        tfile = os.path.join(path, "title.txt")
        if os.path.exists(tfile):
            dirty = must_create_index(path)
            title = open(tfile).read().replace("\n","")
            date = createSubIndex(path, title, dirty)
            foundDirs[path] = date, title
    orderedPaths = sorted(list(foundDirs.keys()), key=lambda k: foundDirs[k][1].lower())
    with open("index.html", "w") as localIndex:
        localIndex.write(render("main_index.html",{
            "orderedPaths": orderedPaths,
            "foundDirs": foundDirs,
            "photodirs": sorted([path for path in glob.glob("photos-*") if hasPhotos(path)]),
            "videos": findVideos(),
        }))
    print("Création de : index.html")
    
def exportXml(path):
    """
    Exporte au format musicXML un fichier reconnu par Musescore s'il le faut
:return: le chemin vers le fichier au format musicXML
    """
    outfile = path + ".xml"
    if not os.path.exists(outfile) or \
       os.path.getmtime(outfile) < os.path.getmtime(path):
        sys.stdout.write(f"Export de {outfile} ... ")
        sys.stdout.flush()
        run(f"musescore {path} -o {outfile}", shell = True,
            capture_output=True, encoding="UTF-8")
        print("[fait]")
    return outfile

def exportMP3(path, directory, outscore=[]):
    """
    Exporte un fichier musical en format MP3
    :param: path chemin vers le fichier à exporter
    :param: directory un dossier où mettre le fichier
    :param: outscore une liste de portées à exporter
    """
    xml = exportXml(path)
    tempfilename = ""
    with tempfile.NamedTemporaryFile(suffix=".xml") as tmpf:
        tempfilename = tmpf.name
    tree = ET.parse(xml)
    doc = tree.getroot()
    for score in doc.findall("part-list/score-part"):
        name = score.find("part-name").text
        vol  = score.find("midi-instrument/volume")
        if name in outscore:
            vol.text = "61"
        else:
            vol.text = "0"
    run(f"rm -f {tempfilename}", shell=True)
    tree.write(tempfilename)
    suffix = "_"+ "_".join(outscore) + ".mp3"
    outfile = os.path.join(directory,
                           os.path.basename(path).split(".")[0] + suffix)
    sys.stdout.write(f"Export de {outfile} ... ")
    sys.stdout.flush()
    run(f"musescore {tempfilename} -o {outfile}", shell = True,
        capture_output=True, encoding="UTF-8")
    print("[fait]")
    return

def partNames(xmlfile):
    """
    Extrait la liste des partitions d'un fichier musicXML
    :param xmlfile: un fichier musicXML
    :return: une liste des noms des portées
    """
    tree = ET.parse(xmlfile)
    doc = tree.getroot()
    result = []
    for score in doc.findall("part-list/score-part"):
        result.append(score.find("part-name").text)
    return result

if __name__ == '__main__':
    createIndex()
    
