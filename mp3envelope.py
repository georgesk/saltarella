#! /usr/bin/python3

"""
Extraction de l'enveloppe d'un son issu d'un fichier MP3
Auteur : Georges Khaznadar <georgesk@debian.org>

dépendances :
-------------
ffmpeg, utilisé comme convertisseur mp3 vers wav
"""

import subprocess, tempfile, io, time
from scipy.io import wavfile
from scipy.signal import find_peaks
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":

    mp3file = "./la-javanaise/mp3_00tous/La Javanaise_basse.mp3"
    cp = subprocess.run(
        ["ffmpeg", '-i', mp3file, "-f", "wav", "-acodec", "pcm_u8",  "-"],
        capture_output=True
    )
    print("cp.stdout", type(cp.stdout), len(cp.stdout))
    wavdata = io.BytesIO(cp.stdout)
    samplerate, data = wavfile.read(wavdata)
    print(f"samplerate = {samplerate}, len(data) = {len(data)}")

    peaks = find_peaks(data)
    
    """
    analytic_signal = hilbert(data)
    print(f"type(analytic_signal) = {type(analytic_signal)}")
    amplitude_envelope = np.abs(analytic_signal)
    print(f"type(amplitude_envelope) = {type(amplitude_envelope)}")
    
    t = np.arange(len(data))
    #plt.plot(t, signal, label='signal')
    print("avant plot")
    start = time.time()
    plt.plot(t, amplitude_envelope, label='envelope')
    plt.show()
    print(f"apres plot ({time.time() - start})")
    """
